package com.learn.mybatis.service;

import java.util.List;

import com.learn.mybatis.bean.Employee;
import com.learn.mybatis.dao.EmployeeMapper;
import org.apache.ibatis.session.SqlSession;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;


@Service
public class EmployeeService {

    @Autowired
    private EmployeeMapper employeeMapper;

    // sqlSession 为applicationContext.xml中指定的批量执行专用sqlSession
    @Autowired
    private SqlSession sqlSessionBatch;

    public List<Employee> getEmps(){
        // 想要批量执行的，用sqlSessionBatch替代默认的sqlSessionFactoryBean，重新创建一个sqlSession
        //EmployeeMapper mapper = sqlSessionBatch.getMapper(EmployeeMapper.class);
        // mapper.getEmps();
        return employeeMapper.getEmps();
    }

}
