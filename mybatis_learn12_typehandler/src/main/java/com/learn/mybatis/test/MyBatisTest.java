package com.learn.mybatis.test;

import java.io.IOException;
import java.io.InputStream;
import java.util.Arrays;
import java.util.List;

import com.github.pagehelper.Page;
import com.github.pagehelper.PageHelper;
import com.github.pagehelper.PageInfo;
import com.learn.mybatis.bean.EmpStatus;
import com.learn.mybatis.bean.Employee;
import com.learn.mybatis.dao.IEmployeeMapper;
import org.apache.ibatis.io.Resources;
import org.apache.ibatis.session.SqlSession;
import org.apache.ibatis.session.SqlSessionFactory;
import org.apache.ibatis.session.SqlSessionFactoryBuilder;
import org.junit.Test;

public class MyBatisTest {

    public SqlSessionFactory getSqlSessionFactory() throws IOException {
        String resource = "mybatis-config.xml";
        InputStream inputStream = Resources.getResourceAsStream(resource);
        return new SqlSessionFactoryBuilder().build(inputStream);
    }

    // 测试下枚举的使用
    @Test
    public void testEnumUse() {
        EmpStatus login = EmpStatus.LOGIN;
        System.out.println("枚举的索引：" + login.ordinal());
        System.out.println("枚举的名字：" + login.name());

//        System.out.println("枚举的状态码：" + login.getCode());
//        System.out.println("枚举的提示消息：" + login.getMsg());
    }

    /**
     * 默认mybatis在处理枚举对象的时候保存的是枚举的名字：EnumTypeHandler
     * 改变使用：EnumOrdinalTypeHandler：
     *
     * @throws IOException
     */
    @Test
    public void testAddEnum() throws IOException {
        SqlSessionFactory sqlSessionFactory = getSqlSessionFactory();
        SqlSession openSession = sqlSessionFactory.openSession();
        try {
            IEmployeeMapper mapper = openSession.getMapper(IEmployeeMapper.class);
            Employee employee = new Employee("test_enum", "test.com", "1");
            mapper.addEmp(employee);
            System.out.println("保存成功" + employee.getId());
            openSession.commit();
        } finally {
            openSession.close();
        }
    }

    @Test
    public void testGetEmpById() throws IOException {
        SqlSessionFactory sqlSessionFactory = getSqlSessionFactory();
        SqlSession openSession = sqlSessionFactory.openSession();
        try {
            IEmployeeMapper mapper = openSession.getMapper(IEmployeeMapper.class);
            Employee e = mapper.getEmpById(37);
            System.out.println(e.toString());
        } finally {
            openSession.close();
        }
    }

    @Test
    public void testSelect() throws IOException {
        SqlSessionFactory sqlSessionFactory = getSqlSessionFactory();
        SqlSession openSession = sqlSessionFactory.openSession();
        try {
            IEmployeeMapper mapper = openSession.getMapper(IEmployeeMapper.class);
            Employee employee = new Employee("test_enum", "test.com", "1");
            List<Employee> emps = mapper.getEmps();
            for (Employee e : emps) {
                System.out.println(e.toString());
            }
        } finally {
            openSession.close();
        }
    }


}