package com.learn.mybatis.test;

import java.io.IOException;
import java.io.InputStream;
import java.util.UUID;

import com.learn.mybatis.bean.Employee;
import com.learn.mybatis.dao.EmployeeMapper;
import org.apache.ibatis.io.Resources;
import org.apache.ibatis.session.ExecutorType;
import org.apache.ibatis.session.SqlSession;
import org.apache.ibatis.session.SqlSessionFactory;
import org.apache.ibatis.session.SqlSessionFactoryBuilder;
import org.junit.Test;

public class MyBatisTest {

    public SqlSessionFactory getSqlSessionFactory() throws IOException {
        String resource = "mybatis-config.xml";
        InputStream inputStream = Resources.getResourceAsStream(resource);
        return new SqlSessionFactoryBuilder().build(inputStream);
    }

    @Test
    public void testBatch() throws IOException {
        SqlSessionFactory sqlSessionFactory = getSqlSessionFactory();

        // 非批量模式
//        SqlSession openSession = sqlSessionFactory.openSession(ExecutorType.BATCH);
        // 指定mybatis的默认执行器为批量
        SqlSession openSession = sqlSessionFactory.openSession(ExecutorType.BATCH);
        long start = System.currentTimeMillis();
        try {
            EmployeeMapper mapper = openSession.getMapper(EmployeeMapper.class);
            for (int i = 0; i < 10000; i++) {
                mapper.addEmp(new Employee(UUID.randomUUID().toString().substring(0, 5) + "-批量名称", "默认邮箱", "1"));
            }
            openSession.commit();
            // 批量模式：数据库预编译sql一次==>数据库等待要传入的参数===>参数集合收集好批量传给数据库===>执行1次，将所有数据处理完毕
            // 非批量模式：(数据库预编译sql一次===>数据库等待要传入的参数===>数据库接收到一条语句的参数===>执行一次) * 10000遍
        } finally {
            openSession.close();
            long end = System.currentTimeMillis();
            System.out.println("执行时长：" + (end - start));
        }

    }

}