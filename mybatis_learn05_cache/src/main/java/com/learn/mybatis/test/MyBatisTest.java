package com.learn.mybatis.controller;

import java.io.IOException;
import java.io.InputStream;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Map;

import com.learn.mybatis.bean.Department;
import com.learn.mybatis.bean.Employee;
import com.learn.mybatis.dao.*;
import org.apache.ibatis.io.Resources;
import org.apache.ibatis.session.SqlSession;
import org.apache.ibatis.session.SqlSessionFactory;
import org.apache.ibatis.session.SqlSessionFactoryBuilder;
import org.junit.Test;

public class MyBatisTest {



    /**
     * 一级缓存：（本地缓存）：sqlSession级别的缓存。
     *      该缓存，本质上就是作用域为SqlSession级别的一个Map对象。当需要从缓存中拿数据时，直接从该Map中获取。
     *          SqlSession作用域级别：一个sqlSession会话对象，独享自己的一级缓存。其它sqlSession绘画对象，不能获取别人的一级缓存内容。
     * 特点：
     *      一级缓存是一直开启的，无法关闭。
     * 		与数据库同一次会话期间查询到的数据会放在本地缓存中。
     * 		以后如果需要获取相同的数据，直接从缓存中拿，没必要再去查询数据库；
     *
     * 一级缓存失效，需要重新sql查询的情况：（没有使用到当前一级缓存的情况，效果就是，还需要再向数据库发出查询）：
     * 		1、sqlSession会话不同。
     * 		2、sqlSession相同，查询条件不同.(当前一级缓存中还没有这个数据)
     * 		3、sqlSession相同，两次查询之间执行了增删改操作(这次增删改可能对当前数据有影响)
     * 		4、sqlSession相同，手动清除了一级缓存（缓存清空）
     * @throws IOException
     *
     */
    @Test
    public void testFirstLevelCache() throws IOException{
        SqlSessionFactory sqlSessionFactory = getSqlSessionFactory();
        try{
            SqlSession openSession = sqlSessionFactory.openSession();
            EmployeeMapper mapper = openSession.getMapper(EmployeeMapper.class);
            Employee emp01 = mapper.getEmpById(1);
            System.out.println(emp01);
            // 重复查询后，发现sql语句日志不会再打印，直接从同一个会话的一级缓存中获取到了目标数据
            Employee emp02 = mapper.getEmpById(1);
            System.out.println(emp01);
            System.out.println(emp01==emp02); // true 前后两个对象完全相等

            // 1、sqlSession不同。
            SqlSession openSession2 = sqlSessionFactory.openSession();
            EmployeeMapper mapper2 = openSession2.getMapper(EmployeeMapper.class);
            Employee emp03 = mapper2.getEmpById(1);
            System.out.println(emp03);
            System.out.println(emp03==emp02); // false 两次会话获取的对象完全不同

            // 2、sqlSession相同，查询条件不同.(当前一级缓存中还没有这个数据)
            Employee emp04 = mapper2.getEmpById(2);
            System.out.println(emp04);

            // 3、sqlSession相同，两次查询之间执行了增删改操作(这次增删改可能对当前数据有影响)
            mapper2.addEmp(new Employee(null, "aaa", "nnn", "0"));
            Employee emp05 = mapper2.getEmpById(2);
            System.out.println(emp05==emp04); // false

            // 4、sqlSession相同，手动清除了一级缓存（缓存清空）
            openSession2.clearCache();
            Employee emp06 = mapper2.getEmpById(2);
            System.out.println(emp06==emp05); // false

            openSession.close();
            openSession2.close();
        }finally{
        }
    }


    /**
     * 二级缓存：（全局缓存）：基于namespace命名空间级别的缓存：一个namespace对应一个二级缓存：
     *      例如：xml文件中的 <mapper namespace="com.learn.mybatis.dao.EmployeeMapperDynamicSQL">
     * 	工作机制：
     * 		1、一个会话，查询一条数据，这个数据就会被放在当前会话的一级缓存中；
     * 		2、如果会话关闭；一级缓存中的数据会被保存到二级缓存中；新的会话查询信息，就可以参照二级缓存中的内容；
     * 		3、二级缓存中保存的信息举例：
     * 	           namespace        被缓存的对象
     * 	           EmployeeMapper   Employee
     * 			   DepartmentMapper  Department
     * 			不同namespace查出的数据会放在自己对应的缓存中（map）
     * 	效果：数据会从二级缓存中获取
     * 		查出的数据都会被默认先放在一级缓存中。
     * 		只有会话提交或者关闭以后，一级缓存中的数据才会转移到二级缓存中
     * 	使用：
     * 		1）、开启全局二级缓存配置：<setting name="cacheEnabled" value="true"/>
     * 		2）、去mapper.xml中添加如下标签，可以开启配置，使用二级缓存：
     * 			<cache></cache>
     * 		    注：该标签有一些默认的策略，
     * 		        例如: <cache eviction="FIFO" flushInterval="60000" readOnly="false" size="1024"></cache>
     *              eviction:缓存的回收策略，缓存过大后，需要删除哪些内容。
     *          		• LRU – 最近最少使用的：移除最长时间不被使用的对象。
     *          		• FIFO – 先进先出：按对象进入缓存的顺序来移除它们。
     *          		• SOFT – 软引用：移除基于垃圾回收器状态和软引用规则的对象。
     *          		• WEAK – 弱引用：更积极地移除基于垃圾收集器状态和弱引用规则的对象。
     *          		• 默认的是 LRU。
     *          	flushInterval：缓存刷新间隔
     *          		缓存多长时间清空一次，默认不清空，设置一个毫秒值
     *          	readOnly:是否只读：
     *          		true：只读；mybatis认为所有从缓存中获取数据的操作都是只读操作，不会修改数据。
     *          				 mybatis为了加快获取速度，直接就会将数据在缓存中的引用交给用户。不安全，用户可以根据引用去修改数据，但是速度快
     *          		false：非只读：mybatis觉得获取的数据可能会被修改。
     *          				mybatis会利用序列化&反序列的技术克隆一份新的数据给你。安全，速度慢
     *          	    默认为false
     *          	size：缓存存放多少元素；
     *          	type=""：指定自定义缓存的全类名；
     *          			实现Cache接口即可；
     *       3）、因为缓存在readOnly=false期间，会利用序列化&反序列的技术，所以我们的POJO需要实现序列化接口
     *
     * @throws IOException
     *
     */
    @Test
    public void testSecondLevelCache() throws IOException{
        SqlSessionFactory sqlSessionFactory = getSqlSessionFactory();
        SqlSession openSession = sqlSessionFactory.openSession();
        SqlSession openSession2 = sqlSessionFactory.openSession();
        try{
            // 首次会话
            EmployeeMapper mapper = openSession.getMapper(EmployeeMapper.class);
            EmployeeMapper mapper2 = openSession2.getMapper(EmployeeMapper.class);
            Employee emp01 = mapper.getEmpById(1);
            System.out.println(emp01);
            // 会话及时关闭，当前会话的数据才会从一级缓存中转移到二级缓存。
            // 如果是放在第二次会话后才关闭，那么二级缓存中找不到目标数据，第二次会话会重新查询。
            openSession.close();
            //第二次查询是从二级缓存中拿到的数据，并没有发送新的sql
            //mapper2.addEmp(new Employee(null, "aaa", "nnn", "0"));
            Employee emp02 = mapper2.getEmpById(1);
            System.out.println(emp02);
            System.out.println(emp02==emp01); // false


            openSession2.close();

        }finally{

        }
    }

    // 测试DepartmentMapper.xml没有开启二级缓存时，两个会话的缓存获取情况
    @Test
    public void testSecondLevelCache02() throws IOException{
        SqlSessionFactory sqlSessionFactory = getSqlSessionFactory();
        SqlSession openSession = sqlSessionFactory.openSession();
        SqlSession openSession2 = sqlSessionFactory.openSession();
        try{
            DepartmentMapper mapper = openSession.getMapper(DepartmentMapper.class);
            DepartmentMapper mapper2 = openSession2.getMapper(DepartmentMapper.class);

            Department deptById = mapper.getDeptById(1);
            System.out.println(deptById);
            openSession.close();
            Department deptById2 = mapper2.getDeptById(1);
            System.out.println(deptById2);
            openSession2.close();

        }finally{

        }
    }


    /**
     *
     * 和缓存有关的设置/属性：
     * 			1）、全局settings：cacheEnabled=true/false：关闭缓存（二级缓存关闭）(一级缓存一直可用的)
     * 			2）、每个select标签都有 useCache="true"：
     * 					false：不使用缓存（一级缓存依然使用，二级缓存不使用）
     * 			3）、每个增删改标签，insert/update/delete ，都可以使用 flushCache="true"，默认值为true。
     * 					增删改执行完成后就会清除一级和二级缓存；
     * 					测试：flushCache="true"：一级缓存就清空了；二级也会被清除；
     * 					查询select标签：默认flushCache="false"：
     * 						如果改flushCache=true;每次查询之后都会清空缓存；缓存是没有被使用的；
     * 			4）、sqlSession.clearCache();只清除当前session的一级缓存，不影响二级缓存。
     * 			5）、全局settings设置中，localCacheScope：本地缓存作用域。
     * 					取值：
     * 				        session，默认。即一级缓存。代表保存当前会话的所有数据到本地缓存中。
     * 				        statement，取这个值，可以禁用一级缓存，即本地缓存中不会保存任何数据。
     * 				    mybatis3.3后，才有该设置。
     * 				一般没人使用。
     */

    public SqlSessionFactory getSqlSessionFactory() throws IOException {
        String resource = "mybatis-config.xml";
        InputStream inputStream = Resources.getResourceAsStream(resource);
        return new SqlSessionFactoryBuilder().build(inputStream);
    }
}