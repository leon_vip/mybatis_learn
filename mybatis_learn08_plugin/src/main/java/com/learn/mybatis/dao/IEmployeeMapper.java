package com.learn.mybatis.dao;

import com.learn.mybatis.bean.Employee;

public interface IEmployeeMapper {

    public Employee getEmpById(Integer id);

}