package com.learn.mybatis.dao;


import org.apache.ibatis.executor.statement.StatementHandler;
import org.apache.ibatis.plugin.*;
import org.apache.ibatis.reflection.MetaObject;
import org.apache.ibatis.reflection.SystemMetaObject;

import java.util.Properties;

/**
 * 自定义插件演示
 * 条件查询时，手动的更改传参：
 *  动态的改变一下sql运行的参数：以前1号员工，实际从数据库查询3号员工
 */
@Intercepts(
        {
                @Signature(type = StatementHandler.class, method = "parameterize", args = java.sql.Statement.class)
        })
public class MySimplePlugin implements Interceptor {

    @Override
    public Object intercept(Invocation invocation) throws Throwable {
        System.out.println("MySimplePlugin拦截器--intercept方法--拦截目标方法:" + invocation.getMethod());

        // invocation 目前拦截的目标对象为接口 StatementHandler 的实现类
        Object target = invocation.getTarget();
        System.out.println("当前拦截到的对象，经过动态代理后为："+target);
        // 通过MetaObject拿到target的元数据
        // 保存SQL参数信息的获取方式为 StatementHandler接口==>属性：parameterHandler===>属性：parameterObject
        MetaObject metaObject = SystemMetaObject.forObject(target);
        Object value = metaObject.getValue("parameterHandler.parameterObject");
        System.out.println("sql语句原本传递的参数是："+value);
        // 修改完sql语句要用的参数
        metaObject.setValue("parameterHandler.parameterObject", 3);

        return invocation.proceed();
    }

    @Override
    public Object plugin(Object target) {
        System.out.println("MySimplePlugin拦截器--plugin方法--开始包装目标对象：" + target);
        return Plugin.wrap(target, this);
    }

    @Override
    public void setProperties(Properties properties) {
        System.out.println("给插件MySimplePlugin（Interceptor拦截器）配置的信息：" + properties);
    }

}
