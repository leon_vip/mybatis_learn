package com.learn.mybatis.dao;

import com.learn.mybatis.bean.Employee;
import com.learn.mybatis.bean.OraclePage;

import java.util.List;

public interface EmployeeMapper {

    public Employee getEmpById(Integer id);

    public List<Employee> getEmps();

    public Long addEmp(Employee employee);

    public void getPageByProcedure(OraclePage page);
}