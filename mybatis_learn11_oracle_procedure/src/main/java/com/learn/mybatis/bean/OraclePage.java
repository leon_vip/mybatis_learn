package com.learn.mybatis.bean;

import java.util.List;

/**
 * 封装分页查询数据
 */
public class OraclePage {

    // 存储过程的输入参数
    private int start;
    private int end;
    // 存储过程的输出参数
    private int count;
    private List<Employee> emps;

    public int getStart() {
        return start;
    }

    public void setStart(int start) {
        this.start = start;
    }

    public int getEnd() {
        return end;
    }

    public void setEnd(int end) {
        this.end = end;
    }

    public int getCount() {
        return count;
    }

    public void setCount(int count) {
        this.count = count;
    }

    public List<Employee> getEmps() {
        return emps;
    }

    public void setEmps(List<Employee> emps) {
        this.emps = emps;
    }


}