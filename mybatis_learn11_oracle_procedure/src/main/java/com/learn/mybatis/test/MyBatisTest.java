package com.learn.mybatis.test;

import java.io.IOException;
import java.io.InputStream;
import java.util.UUID;

import com.learn.mybatis.bean.Employee;
import com.learn.mybatis.bean.OraclePage;
import com.learn.mybatis.dao.EmployeeMapper;
import org.apache.ibatis.io.Resources;
import org.apache.ibatis.session.ExecutorType;
import org.apache.ibatis.session.SqlSession;
import org.apache.ibatis.session.SqlSessionFactory;
import org.apache.ibatis.session.SqlSessionFactoryBuilder;
import org.junit.Test;

public class MyBatisTest {

    public SqlSessionFactory getSqlSessionFactory() throws IOException {
        String resource = "mybatis-config.xml";
        InputStream inputStream = Resources.getResourceAsStream(resource);
        return new SqlSessionFactoryBuilder().build(inputStream);
    }

    /**
     * oracle分页：
     * 		借助rownum：行号；子查询；
     * 存储过程包装分页逻辑
     * @throws IOException
     */
    @Test
    public void testProcedure() throws IOException{
        SqlSessionFactory sqlSessionFactory = getSqlSessionFactory();
        SqlSession openSession = sqlSessionFactory.openSession();
        try{
            EmployeeMapper mapper = openSession.getMapper(EmployeeMapper.class);
            OraclePage page = new OraclePage();
            page.setStart(1);
            page.setEnd(5);
            mapper.getPageByProcedure(page);

            System.out.println("总记录数："+page.getCount());
            System.out.println("查出的数据："+page.getEmps().size());
            System.out.println("查出的数据："+page.getEmps());
        }finally{
            openSession.close();
        }

    }

}