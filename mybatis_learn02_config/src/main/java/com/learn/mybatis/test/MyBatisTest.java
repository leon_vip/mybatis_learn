package com.learn.mybatis.controller;

import java.io.IOException;
import java.io.InputStream;

import com.learn.mybatis.bean.Employee;
import com.learn.mybatis.dao.EmployeeMapperAnnotation;
import com.learn.mybatis.dao.EmployeeMapper;
import org.apache.ibatis.io.Resources;
import org.apache.ibatis.session.SqlSession;
import org.apache.ibatis.session.SqlSessionFactory;
import org.apache.ibatis.session.SqlSessionFactoryBuilder;
import org.junit.Test;

public class MyBatisTest {

    /**
     * 简单案例的基本思路
     * 1、根据xml配置文件（全局配置文件）创建一个SqlSessionFactory对象 有数据库连接池信息、事务管理器信息等运行环境信息
     * 2、sql映射文件；配置了每一个sql，以及sql的封装规则等。
     * 3、将sql映射文件注册在全局配置文件中
     * 4、写代码：
     * 		1）、根据全局配置文件得到SqlSessionFactory；
     * 		2）、使用sqlSession工厂，获取到sqlSession对象使用他来执行增删改查
     * 			一个sqlSession就是代表和数据库的一次会话，用完关闭
     * 		3）、使用sql的唯一标志来告诉MyBatis执行哪个sql。sql都是保存在sql映射文件中的。
     *
     * @throws IOException
     */
    @Test
    public void test01() throws IOException {
        // 1、根据xml全局配置文件mybatis-config.xml，中的数据源等运行环境信息，创建一个SqlSessionFactory对象。
        String resource = "mybatis-config.xml";
        InputStream inputStream = Resources.getResourceAsStream(resource);
        SqlSessionFactory sqlSessionFactory = new SqlSessionFactoryBuilder().build(inputStream);

        // 2 使用sqlSession工厂，获取到sqlSession对象使用他来执行增删改查，
        // 一个sqlSession就是代表和数据库的一次会话，用完关闭
        SqlSession openSession = sqlSessionFactory.openSession();

        // 3、在 SqlSession实例中，执行已经提前准备好映射关系的sql语句。
        //  使用sql的唯一标志来告诉MyBatis执行哪个sql。sql都是保存在sql映射文件中的。
        // selectOne() 查询单条记录
        // 第一个参数：sql语句的唯一标识符。为避免Id重复，所以加上配置文件的namespace作为前缀。
        // 第二个参数：存放执行sql语句的传递参数
        Employee e = openSession.selectOne("com.learn.mybatis.dao.EmployeeMapper.getEmpById",1);
        System.out.println(e.toString());
        // SqlSession代表和数据库的一次会话；用完必须关闭；
        openSession.close();
    }


    public SqlSessionFactory getSqlSessionFactory() throws IOException {
        String resource = "mybatis-config.xml";
        InputStream inputStream = Resources.getResourceAsStream(resource);
        return new SqlSessionFactoryBuilder().build(inputStream);
    }

    /**
     * 接口式编程
     * @throws IOException
     */
    @Test
    public void test02() throws IOException {
        // 1、获取sqlSessionFactory对象
        SqlSessionFactory sqlSessionFactory = getSqlSessionFactory();
        // 2、获取sqlSession对象
        SqlSession openSession = sqlSessionFactory.openSession();
        try {
            // 3、根据配置文件，获取指定接口的实现类对象
            // 会为接口自动的创建一个代理对象，将接口与xml配置绑定。代理对象去执行增删改查方法
            EmployeeMapper mapper = openSession.getMapper(EmployeeMapper.class);
            Employee employee = mapper.getEmpById(1);
            System.out.println(mapper.getClass());
            System.out.println(employee);
        } finally {
            openSession.close();
        }

    }

    // 测试sql映射文件，接口式编程，利用接口注解赖声明sql
    @Test
    public void test03() throws IOException{
        SqlSessionFactory sqlSessionFactory = getSqlSessionFactory();
        SqlSession openSession = sqlSessionFactory.openSession();
        try{
            EmployeeMapperAnnotation mapper = openSession.getMapper(EmployeeMapperAnnotation.class);
            Employee empById = mapper.getEmpById(1);
            System.out.println(empById);
        }finally{
            openSession.close();
        }
    }

}