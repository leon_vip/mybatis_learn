package com.learn.mybatis.dao;

import com.learn.mybatis.bean.Employee;

public interface EmployeeMapper {

    public Employee getEmpById(Integer id);

}