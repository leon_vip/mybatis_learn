<?xml version="1.0" encoding="UTF-8" ?>
<!DOCTYPE configuration
        PUBLIC "-//mybatis.org//DTD Config 3.0//EN"
        "http://mybatis.org/dtd/mybatis-3-config.dtd">
<configuration>
    <!--
        1、mybatis可以使用properties来引入外部properties配置文件的内容；
        resource：引入类路径下的资源
        url：引入网络路径或者磁盘路径下的资源

        # 如果直接在conf下，则为
        <properties resource="dbconfig.properties></properties>
        # 如果是conf的多级目录，则为
        <properties resource="com/atguigu/dbconfig.properties></properties>
      -->
    <properties resource="dbconfig.properties"></properties>


    <!--
        2、settings包含很多重要的设置项
            setting:用来设置每一个设置项
                name：设置项名
                value：设置项取值
          设置项有很多，用到时再补充。
     -->
    <settings>
        <!--开启自动映射的功能。
            唯一的要求是列名和javaBean属性名一致
            如果autoMappingBehavior设置为null则会取消自动映射-->
        <setting name="autoMappingBehavior" value="PARTIAL"/>
        <!--是否开启自动驼峰命名规则映射。
            生效的条件：数据库字段命名规范，POJO属性符合驼峰命名法
            默认为false，不开启。
            设置为true，即数据库字段：A_MAN，java属性字段可以为aMan
        -->
        <setting name="mapUnderscoreToCamelCase" value="true"/>
        <!--mybatis对所有的null都映射的是原生Jdbc的OTHER类型，oracle不能正确处理
            但是mysql支持该类型，所以mysql数据库不会报错。
            设置为NULL后，提升兼容性-->
        <setting name="jdbcTypeForNull" value="NULL"/>



        <!--显示的指定每个我们需要更改的配置的值，即使他是默认的。防止版本更新带来的问题  -->
        <!--开启懒加载
            开启后，sql映射文件中，关联查询会延迟加载，需要用到时再加载。
            默认false，不开启-->
        <setting name="lazyLoadingEnabled" value="true"/>
        <!--侵入懒加载
            默认true，开启时，当需要bean的任何一个属性时，所有属性都会被加载。
            false，关闭时，只会在需要这个属性时，才会加载对应的属性。-->
        <setting name="aggressiveLazyLoading" value="false"/>
    </settings>


    <!--3、typeAliases：别名处理器：可以为我们的java类型起别名
		别名不区分大小写
	-->
    <typeAliases>
        <!-- 1、typeAlias:为某个java类型起别名
                type:指定要起别名的类型全类名; 默认别名就是类名小写；employee
                alias:指定新的别名
         -->
        <!-- <typeAlias type="com.learn.mybatis.bean.Employee" alias="emp"/> -->

        <!-- 2、package:为某个包下的所有类批量起别名
                name：指定包名（为当前包以及下面所有的后代包的，每一个类都起一个默认别名（类名小写），）
        -->
        <package name="com.learn.mybatis.bean"/>

        <!-- 3、批量起别名的情况下，使用@Alias注解为某个类型指定新的别名 -->
    </typeAliases>

    <!--4、多种环境。 mybatis可以配置多种环境
        default指定使用某种环境。可以达到快速切换环境。
    -->
    <environments default="dev_mysql">
        <!--environment：配置一个具体的环境信息；
            必须有两个标签 transactionManager、dataSource；
            id代表当前环境的唯一标识
        -->
        <environment id="dev_mysql">
            <!--transactionManager：事务管理器；
                type：事务管理器的类型;
                    一般有2种取值：
                        JDBC(JdbcTransactionFactory) 通过JDBC容器的方式进行事务控制
                        MANAGED(ManagedTransactionFactory) 通过J2EE事务容器的方式进行事务控制
                    也可以 自定义事务管理器：想JdbcTransactionFactory类似，实现TransactionFactory接口.这时type指定为全类名
                与spring整合后，事务一般都交给spring去控制。所以这里了解即可。
            -->
            <transactionManager type="JDBC"></transactionManager>
            <!--dataSource：配置数据源信息;
                type:数据源类型;
                一般有3中取值：
                    UNPOOLED(UnpooledDataSourceFactory)
                    POOLED(PooledDataSourceFactory)
                    JNDI(JndiDataSourceFactory)
                也可以 自定义数据源，（例如c3p0、dbcp）：实现DataSourceFactory接口，这时type是全类名
            -->
            <dataSource type="POOLED">
                <property name="driver" value="${jdbc.driver}"/>
                <property name="url" value="${jdbc.url}"/>
                <property name="username" value="${jdbc.username}"/>
                <property name="password" value="${jdbc.password}"/>
            </dataSource>
        </environment>

        <environment id="dev_oracle">
            <transactionManager type="JDBC"/>
            <dataSource type="POOLED">
                <property name="driver" value="${orcl.driver}"/>
                <property name="url" value="${orcl.url}"/>
                <property name="username" value="${orcl.username}"/>
                <property name="password" value="${orcl.password}"/>
            </dataSource>
        </environment>
    </environments>

    <!-- 5、databaseIdProvider：为数据库厂商标识起别名，方便调用。
        支持多数据库厂商的；
         type="DB_VENDOR"：
            DB_VENDOR也是别名，实际对应 VendorDatabaseIdProvider
            作用就是先得到数据库厂商的标识，mybatis就能根据数据库厂商标识来执行不同的sql;
            数据库驱动的获取标识方法为 getDatabaseProductName()
            实际MYSQL数据库的标识为 MySQL
            Oracle数据库的标识为 Oracle
            SQL Server数据库的标识为 SQL Server
            ...
  -->
    <databaseIdProvider type="DB_VENDOR">
        <!-- 为不同的数据库厂商起别名 -->
        <property name="MySQL" value="mysql"/>
        <property name="Oracle" value="oracle"/>
        <property name="SQL Server" value="sqlserver"/>
    </databaseIdProvider>


    <!-- 将我们写好的sql映射文件（EmployeeMapper.xml）一定要注册到全局配置文件（mybatis-config.xml）中 -->
    <!-- 6、mappers：将sql映射注册到全局配置中 -->
    <mappers>
        <!--
            mapper:注册一个sql映射
            注册的映射文件，不能重复。
            三种方式：
                1、单个注册-注册配置文件
                    resource：引用类路径下的sql映射文件
                        例如 <mapper resource="EmployeeMapper.xml"/>
                        通常会将映射文件放到conf源文件夹下的包中。 <mapper resource="mybatis/mapper/EmployeeMapper.xml"/>
                    url：引用网路路径或者磁盘路径下的sql映射文件
                        例如 <mapper url="file:///var/mappers/AuthorMapper.xml"/>


                2、单个注册-注册接口
                    class：引用（注册）接口，
                    两种方式：
                        1、有sql映射xml文件，映射文件名必须和接口同名，并且放在与接口同一目录下，例如./src/main/com/learn/mybatis/dao/
                            但，src和conf都是源码文件夹，源码文件夹下所有的内容，在编译后都会被合并到项目的bin目录下。
                            所以xml可放在conf下的同名包路径中。
                            <mapper class="com.learn.mybatis.dao.EmployeeMapper"/>
                        2、没有sql映射文件，所有的sql都是利用注解写在接口上，例如;
                            <mapper class="com.learn.mybatis.dao.EmployeeMapperAnnotation"/>
                            @Select("select id,last_name,email,gender from tbl_employee where id = #{id}")
                            public Employee getEmpById(Integer id);

                        最终推荐：
                            比较重要的，复杂的Dao接口我们来写sql映射文件
                            不重要的，简单的Dao接口为了开发快速可以使用注解；
                3、批量注册
                    mapper只能引入单个文件，项目大了要配很多，可以指定包路径来批量注册。
                    该包路径指的是src/main/java下的包路径
                    <package name="com.learn.mybatis.dao"/>
                    两种情况：
                        1、有sql映射xml文件，映射文件名必须和接口同名，并且放在与接口同一目录下，例如./src/main/com/learn/mybatis/dao/
                            但，src和conf都是源码文件夹，源码文件夹下所有的内容，在编译后都会被合并到项目的bin目录下。
                            所以xml可放在conf下的同名包路径中。
                        2、没有sql映射文件，所有的sql都是利用注解写在接口上的，可以直接支持
        -->
        <package name="com.learn.mybatis.dao"/>
    </mappers>
</configuration>











