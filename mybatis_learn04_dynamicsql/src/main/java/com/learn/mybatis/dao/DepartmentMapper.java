package com.learn.mybatis.dao;

import com.learn.mybatis.bean.Department;

public interface DepartmentMapper {
    public Department getDeptById(Integer id);

    // 关联查询部门下的人员列表--sql嵌套查询
    public Department getDeptByIdPlus(Integer id);

    // 关联查询部门下的人员列表--分布查询
    public Department getDeptByIdStep(Integer id);

}
