package com.learn.mybatis.dao;

import com.learn.mybatis.bean.Employee;
import org.apache.ibatis.annotations.Param;

import java.util.List;

public interface EmployeeMapperDynamicSQL {

    // if用法演示，查询员工列表，参数对象携带了什么属性就查什么。
    public List<Employee> getEmpsByConditionIf(Employee employee);

    // if用法演示，利用where标签避免where后拼接and
    public List<Employee> getEmpsByConditionWhere(Employee employee);

    // trim用法演示
    public List<Employee> getEmpsByConditionTrim(Employee employee);

    // choose用法演示
    public List<Employee> getEmpsByConditionChoose(Employee employee);

    // 按照ID的值，更新员工员工信息，演示set标签
    public void updateEmpBySet(Employee employee);

    public void updateEmpByTrim(Employee employee);

    //foreach批量查询，查询id集合对应的员工列表
    public List<Employee> getEmpsByConditionForeach(@Param("ids")List<Integer> ids);

    // foreach 批量保存,批量新增员工
    public void addEmpsForeach_mysql_01(@Param("emps")List<Employee> emps);
    public void addEmpsForeach_mysql_02(@Param("emps")List<Employee> emps);
    public void addEmpsForeach_oracle_01(@Param("emps")List<Employee> emps);
    public void addEmpsForeach_oracle_02(@Param("emps")List<Employee> emps);

    // 测试内置参数 _parameter _databaseId 的使用
    public List<Employee> getEmpsTestInnerParameter(Employee employee);
    public List<Employee> getEmpsTestInnerParameterWithBind(Employee employee);

    // 测试通过include sql模板创建sql
    public void addEmpsForeach_mysql_01_ByIncludeSql(@Param("emps")List<Employee> emps);
    public void addEmpsForeach_oracle_02_ByIncludeSql(@Param("emps")List<Employee> emps);
}
