package com.learn.mybatis.controller;

import java.io.IOException;
import java.io.InputStream;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Map;

import com.learn.mybatis.bean.Department;
import com.learn.mybatis.bean.Employee;
import com.learn.mybatis.dao.*;
import org.apache.ibatis.io.Resources;
import org.apache.ibatis.session.SqlSession;
import org.apache.ibatis.session.SqlSessionFactory;
import org.apache.ibatis.session.SqlSessionFactoryBuilder;
import org.junit.Test;

public class MyBatisTest {

    /**
     * 简单案例的基本思路
     * 1、根据xml配置文件（全局配置文件）创建一个SqlSessionFactory对象 有数据库连接池信息、事务管理器信息等运行环境信息
     * 2、sql映射文件；配置了每一个sql，以及sql的封装规则等。
     * 3、将sql映射文件注册在全局配置文件中
     * 4、写代码：
     * 1）、根据全局配置文件得到SqlSessionFactory；
     * 2）、使用sqlSession工厂，获取到sqlSession对象使用他来执行增删改查
     * 一个sqlSession就是代表和数据库的一次会话，用完关闭
     * 3）、使用sql的唯一标志来告诉MyBatis执行哪个sql。sql都是保存在sql映射文件中的。
     *
     * @throws IOException
     */
    @Test
    public void test01() throws IOException {
        // 1、根据xml全局配置文件mybatis-config.xml，中的数据源等运行环境信息，创建一个SqlSessionFactory对象。
        String resource = "mybatis-config.xml";
        InputStream inputStream = Resources.getResourceAsStream(resource);
        SqlSessionFactory sqlSessionFactory = new SqlSessionFactoryBuilder().build(inputStream);

        // 2 使用sqlSession工厂，获取到sqlSession对象使用他来执行增删改查，
        // 一个sqlSession就是代表和数据库的一次会话，用完关闭
        SqlSession openSession = sqlSessionFactory.openSession();

        // 3、在 SqlSession实例中，执行已经提前准备好映射关系的sql语句。
        //  使用sql的唯一标志来告诉MyBatis执行哪个sql。sql都是保存在sql映射文件中的。
        // selectOne() 查询单条记录
        // 第一个参数：sql语句的唯一标识符。为避免Id重复，所以加上配置文件的namespace作为前缀。
        // 第二个参数：存放执行sql语句的传递参数
        Employee e = openSession.selectOne("com.learn.mybatis.dao.EmployeeMapper.getEmpById", 1);
        System.out.println(e.toString());
        // SqlSession代表和数据库的一次会话；用完必须关闭；
        openSession.close();
    }


    public SqlSessionFactory getSqlSessionFactory() throws IOException {
        String resource = "mybatis-config.xml";
        InputStream inputStream = Resources.getResourceAsStream(resource);
        return new SqlSessionFactoryBuilder().build(inputStream);
    }

    /**
     * 接口式编程
     *
     * @throws IOException
     */
    @Test
    public void test02() throws IOException {
        // 1、获取sqlSessionFactory对象
        SqlSessionFactory sqlSessionFactory = getSqlSessionFactory();
        // 2、获取sqlSession对象
        SqlSession openSession = sqlSessionFactory.openSession();
        try {
            // 3、根据配置文件，获取指定接口的实现类对象
            // 会为接口自动的创建一个代理对象，将接口与xml配置绑定。代理对象去执行增删改查方法
            EmployeeMapper mapper = openSession.getMapper(EmployeeMapper.class);
            Employee employee = mapper.getEmpById(1);
            System.out.println(mapper.getClass());
            System.out.println(employee);
        } finally {
            openSession.close();
        }

    }

    // 测试sql映射文件，接口式编程，利用接口注解赖声明sql
    @Test
    public void test03() throws IOException {
        SqlSessionFactory sqlSessionFactory = getSqlSessionFactory();
        SqlSession openSession = sqlSessionFactory.openSession();
        try {
            EmployeeMapperAnnotation mapper = openSession.getMapper(EmployeeMapperAnnotation.class);
            Employee empById = mapper.getEmpById(1);
            System.out.println(empById);
        } finally {
            openSession.close();
        }
    }


    /**
     * 测试增删改
     * 1、mybatis允许增删改直接在接口方法定义以下类型返回值
     * Integer、Long、Boolean、void
     * 2、我们需要手动提交数据
     * sqlSessionFactory.openSession();===》手动提交
     * 或
     * sqlSessionFactory.openSession(true);===》自动提交
     *
     * @throws IOException
     */
    @Test
    public void test04() throws IOException {

        SqlSessionFactory sqlSessionFactory = getSqlSessionFactory();
        //1、获取到的SqlSession不会自动提交数据
        SqlSession openSession = sqlSessionFactory.openSession();

        try {
            EmployeeMapper mapper = openSession.getMapper(EmployeeMapper.class);
            //测试添加
            Employee employee = new Employee(null, "jerry4", null, "1");
            mapper.addEmp(employee);
            System.out.println("返回值为" + employee.getId());

            //测试修改
//            Employee employee = new Employee(1, "Tom", "jerry@atguigu.com", "0");
//            boolean updateEmp = mapper.updateEmp(employee);
//            System.out.println("返回值为"+updateEmp);
            //测试删除
//            mapper.deleteEmpById(1);
            //2、手动提交数据
            openSession.commit();
        } finally {
            openSession.close();
        }

    }


    @Test
    public void test05() throws IOException {

        SqlSessionFactory sqlSessionFactory = getSqlSessionFactory();
        //1、获取到的SqlSession不会自动提交数据
        SqlSession openSession = sqlSessionFactory.openSession();

        try {
            EmployeeMapper mapper = openSession.getMapper(EmployeeMapper.class);
            // 1 sql传参为，单个参数：mybatis不会做特殊处理，#{}任意名称都行
//			List<Employee> like = mapper.getEmpsByLastNameLike("tom");
//			for (Employee employee : like) {
//				System.out.println(employee);
//			}
            // 2 多个参数：接口参数不加注释
//            Employee employee = mapper.getEmpByIdAndLastName1(1, "tom");
//            System.out.println(employee);

            // 3 命名参数，接口参数加上@Param注释
//            Employee employee = mapper.getEmpByIdAndLastName2(1, "tom");
//            System.out.println(employee);

            //4 参数设为pojo
//            Employee employee = new Employee(1, "Tom", "jerry@atguigu.com", "0");
//            boolean updateEmp = mapper.updateEmp(employee);
//            System.out.println("返回值为"+updateEmp);

            // 5 参数设为map
//            Map<String, Object> map = new HashMap<>();
//            map.put("id", 1);
//            map.put("lastName", "tom");
//            map.put("tableName", "tbl_employee");
//            Employee employee = mapper.getEmpByMap(map);
//            System.out.println(employee);

            // 6 模糊查询，返回列表
//			List<Employee> like = mapper.getEmpsByLastNameLike("%e%");
//			for (Employee employee : like) {
//				System.out.println(employee);
//			}

            // 7 查询并返回一个Map
//            Map<String, Object> map = mapper.getEmpByIdReturnMap(1);
//            System.out.println(map); // {gender=1, last_name=tom, id=1}

            // 8 测试多条记录封装在一个Map中，并自定义map的key
            Map<String, Employee> map = mapper.getEmpByLastNameLikeReturnMap("%j%");
            System.out.println(map); // {2=Employee [id=2, lastName=jerry4, email=1, gender=1], 3=Employee [id=3, lastName=jerry4, email=1, gender=1], 5=Employee [id=5, lastName=jerry4, email=1, gender=1], 6=Employee [id=6, lastName=jerry4, email=1, gender=1]}

        } finally {
            openSession.close();
        }
    }

    @Test
    public void test06() throws IOException {
        SqlSessionFactory sqlSessionFactory = getSqlSessionFactory();
        SqlSession openSession = sqlSessionFactory.openSession();
        try {
            EmployeeMapperPlus mapper = openSession.getMapper(EmployeeMapperPlus.class);
            // 1 设置resultMap，自定义映射规则
//            Employee empById = mapper.getEmpById(1);
//            System.out.println(empById);

            // 2 联合查询：
            // 采用级联属性封装结果集
            // 或
            // 使用association定义关联的单个对象的封装规则；
//            Employee empAndDept = mapper.getEmpAndDept(1);
//            System.out.println(empAndDept);
//            System.out.println(empAndDept.getDept());

            // 3 使用association进行分步查询：
//            Employee employee = mapper.getEmpByIdStep(1);
//            System.out.println(employee);
//            System.out.println(employee.getDept());

            // 4 测试懒加载打开后的查询
//            Employee employee = mapper.getEmpByIdStep(1);
////            System.out.println(employee.getLastName()); // 懒加载只查询 tbl_employee
//            System.out.println(employee); // 用到整个对象时，懒加载查询 整个 tbl_employee 和 tbl_dept


            // 5 测试鉴定器
            Employee employee = mapper.getEmpByIdStep_discriminator(2);
            System.out.println(employee);
            System.out.println(employee.getDept());

        } finally {
            openSession.close();
        }
    }


    @Test
    public void test07() throws IOException {
        SqlSessionFactory sqlSessionFactory = getSqlSessionFactory();
        SqlSession openSession = sqlSessionFactory.openSession();

        try {
            DepartmentMapper mapper = openSession.getMapper(DepartmentMapper.class);
            // 查询部门的时候将部门对应的所有员工信息也查询出来
            // 测试关联查询，属性为list集合
//			Department department = mapper.getDeptByIdPlus(1);
//			System.out.println(department);
//			System.out.println(department.getEmps());

            // 测试分段查询
            Department deptByIdStep = mapper.getDeptByIdStep(1);
            System.out.println(deptByIdStep.getDepartmentName()); // 可以测试懒加载
//            System.out.println(deptByIdStep.getEmps());
        } finally {
            openSession.close();
        }
    }

    /**
     * 测试动态SQL
     *
     * @throws IOException
     */
    @Test
    public void test08() throws IOException {
        SqlSessionFactory sqlSessionFactory = getSqlSessionFactory();
        SqlSession openSession = sqlSessionFactory.openSession();
        try {
            EmployeeMapperDynamicSQL mapper = openSession.getMapper(EmployeeMapperDynamicSQL.class);
            Employee employee = new Employee(1, null, "%qq%", null);
            // 1 if 演示
            //List<Employee> emps = mapper.getEmpsByConditionIf1(employee);

            // 2 if 配合where 演示
            /**
             * 查询的时候，如果某些条件没带，可能sql拼装会有问题，例如 where and ....
             * 1、给where后面加上1=1，以后的条件都and xxx.
             * 2、（mybatis推荐）mybatis使用where标签来将所有的查询条件包括在内。
             *    mybatis就会将where标签中拼装的sql，多出来的and或者or去掉
             *    注意：where只会去掉第一个if判断中，前面多出来的and或者or。 其余的if被触发，前后的and/or依然会被带上
             */
//            List<Employee> emps = mapper.getEmpsByConditionWhere(employee);
            // 3 if 配合 trim 演示
//            List<Employee> emps = mapper.getEmpsByConditionTrim(employee);
            // 4 choose 演示
//            List<Employee> emps = mapper.getEmpsByConditionChoose(employee);
//            for (Employee e : emps) {
//                System.out.println("员工信息：" + e.toString());
//            }

//            Employee employee_update = new Employee(1, "Admin", null, null);
            // 5 通过set标签进行更新操作
//            mapper.updateEmpBySet(employee_update);
            // 6 通过trim标签进行更新操作，与set做对比
//            mapper.updateEmpByTrim(employee_update);
//            openSession.commit();
            // 7 foreach 批量查询
//            List<Employee> list = mapper.getEmpsByConditionForeach(Arrays.asList(1,2));
//            for (Employee emp : list) {
//                System.out.println(emp);
//            }
            // 8 foreach 批量更新
            List<Employee> emps = new ArrayList<>();
            emps.add(new Employee(null, "smith", "smith@qq.com", "1",new Department(1)));
            emps.add(new Employee(null, "allen", "allen@qq.com", "0",new Department(1)));
            mapper.addEmpsForeach_mysql_01(emps);
            openSession.commit();



        } finally {
            openSession.close();
        }
    }


    /**
     * 测试内置参数 _parameter _databaseId 的使用
     * @throws IOException
     */
    @Test
    public void testInnerParam() throws IOException{
        SqlSessionFactory sqlSessionFactory = getSqlSessionFactory();
        SqlSession openSession = sqlSessionFactory.openSession();
        try{
            EmployeeMapperDynamicSQL mapper = openSession.getMapper(EmployeeMapperDynamicSQL.class);
            Employee employee2 = new Employee();
//            employee2.setLastName("%e%");
//            List<Employee> list = mapper.getEmpsTestInnerParameter(employee2);
            employee2.setLastName("e");
            List<Employee> list = mapper.getEmpsTestInnerParameterWithBind(employee2);
            for (Employee employee : list) {
                System.out.println(employee);
            }
        }finally{
            openSession.close();
        }
    }


    /**
     * 测试sql标签模板的使用
     * @throws IOException
     */
    @Test
    public void testSqlTemplate() throws IOException{
        SqlSessionFactory sqlSessionFactory = getSqlSessionFactory();
        SqlSession openSession = sqlSessionFactory.openSession();
        try {
            EmployeeMapperDynamicSQL mapper = openSession.getMapper(EmployeeMapperDynamicSQL.class);
            Employee employee = new Employee(1, null, "%qq%", null);
            List<Employee> emps = new ArrayList<>();
            emps.add(new Employee(null, "smith", "smith@qq.com", "1",new Department(1)));
            emps.add(new Employee(null, "allen", "allen@qq.com", "0",new Department(1)));
            mapper.addEmpsForeach_mysql_01_ByIncludeSql(emps);
            openSession.commit();
        } finally {
            openSession.close();
        }
    }



}