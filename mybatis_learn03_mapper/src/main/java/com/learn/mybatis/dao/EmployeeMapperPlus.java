package com.learn.mybatis.dao;


import com.learn.mybatis.bean.Employee;

import java.util.List;

public interface EmployeeMapperPlus {

    public Employee getEmpById(Integer id);

    public Employee getEmpAndDept(Integer id);

    // 分步查询
    public Employee getEmpByIdStep(Integer id);

    public List<Employee> getEmpsByDeptId(Integer deptId);

    // 测试监听器
    public Employee getEmpByIdStep_discriminator(Integer deptId);
}
