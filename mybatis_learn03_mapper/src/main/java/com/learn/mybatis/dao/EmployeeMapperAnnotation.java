package com.learn.mybatis.dao;

import com.learn.mybatis.bean.Employee;
import org.apache.ibatis.annotations.Select;


public interface EmployeeMapperAnnotation {

//    @Insert()
//    @Delete()
//    @Update()
    @Select("select id,last_name,email,gender from tbl_employee where id = #{id}")
    public Employee getEmpById(Integer id);
}
