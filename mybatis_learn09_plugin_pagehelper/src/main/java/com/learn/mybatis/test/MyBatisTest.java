package com.learn.mybatis.test;

import java.io.IOException;
import java.io.InputStream;
import java.util.Arrays;
import java.util.List;

import com.github.pagehelper.Page;
import com.github.pagehelper.PageHelper;
import com.github.pagehelper.PageInfo;
import com.learn.mybatis.bean.Employee;
import com.learn.mybatis.dao.IEmployeeMapper;
import org.apache.ibatis.io.Resources;
import org.apache.ibatis.session.SqlSession;
import org.apache.ibatis.session.SqlSessionFactory;
import org.apache.ibatis.session.SqlSessionFactoryBuilder;
import org.junit.Test;

public class MyBatisTest {

    public SqlSessionFactory getSqlSessionFactory() throws IOException {
        String resource = "mybatis-config.xml";
        InputStream inputStream = Resources.getResourceAsStream(resource);
        return new SqlSessionFactoryBuilder().build(inputStream);
    }

    @Test
    public void test01() throws IOException {
        // 1、获取sqlSessionFactory对象
        SqlSessionFactory sqlSessionFactory = getSqlSessionFactory();
        // 2、获取sqlSession对象
        SqlSession openSession = sqlSessionFactory.openSession();
        try {
            IEmployeeMapper mapper = openSession.getMapper(IEmployeeMapper.class);
            // 原生mybatis获取所有数据的方法
            List<Employee> emps = mapper.getEmps();
            for(Employee e : emps){
                System.out.println(e.toString());
            }


            // 利用 PageHelper 进行分页查询， pageNum 页码  pageSize 每页显示数据量
            // 采用官方说明中的第二种方法：Mapper接口方式的调用，推荐这种使用方式。
            System.out.println("-----利用 PageHelper 进行分页查询");
            PageHelper.startPage(1, 5);
            List<Employee> emps2 = mapper.getEmps();
            for(Employee e : emps2){
                System.out.println(e.toString());
            }

            // 还可以获取分页的页码相关数据
            System.out.println("-----还可以获取分页的页码相关数据");
            Page<Object> page = PageHelper.startPage(1, 5);
            List<Employee> emps3 = mapper.getEmps();
            for(Employee e : emps3){
                System.out.println(e.toString());
            }
            System.out.println("PageHelper--当前页码："+page.getPageNum());
            System.out.println("PageHelper--总记录数："+page.getTotal());
			System.out.println("PageHelper--每页的记录数："+page.getPageSize());
            System.out.println("PageHelper--总页码："+page.getPages());

            // 利用 PageInfo 进行分页查询，
            System.out.println("-----利用 PageInfo 进行分页查询");
            PageHelper.startPage(2, 5);
            List<Employee> emps4 = mapper.getEmps();
            PageInfo<Employee> pageInfo = new PageInfo<>(emps4);
            System.out.println("pageInfo--当前页码："+pageInfo.getPageNum());
            System.out.println("pageInfo--总记录数："+pageInfo.getTotal());
            System.out.println("pageInfo--每页的记录数："+pageInfo.getPageSize());
            System.out.println("pageInfo--总页码："+pageInfo.getPages());
            System.out.println("pageInfo--是否第一页：" + pageInfo.isIsFirstPage());


            // 利用 PageInfo 获取连续显示多少页时，页码的集合，当前页页码居中
            // 连续分页显示，即当前页码尽量居中显示
            // 当前是第一页，页码提示框为1 2 3 4 5
            // 当前是第二页，页面提示框为 1 2 3 4 5
            // 当前是第三页，页面提示框为 1 2 3 4 5
            // 当前是第四页，页面提示框为 2 3 4 5 6
            System.out.println("-----利用 PageInfo 获取连续显示多少页时，页码的集合");
            PageHelper.startPage(4, 2); // 数据量不多，设置每页只显示2条数据，方便分页显示
            List<Employee> emps5 = mapper.getEmps();
            pageInfo = new PageInfo<>(emps5,5);  // navigatePages代表要显示多少个分页页码
            int[] nums = pageInfo.getNavigatepageNums(); // 获取连续显示的页码集合
            System.out.println("pageInfo--连续显示的页码为：");
            for (int i = 0; i < nums.length; i++) {
                System.out.println(nums[i]);
            }


        } finally {
            openSession.close();
        }

    }


}